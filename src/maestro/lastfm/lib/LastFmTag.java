package maestro.lastfm.lib;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Artyom on 27.08.2014.
 */
public class LastFmTag implements Parcelable {

    private JSONObject jObj;

    public LastFmTag(JSONObject _jObj) {
        jObj = _jObj;
    }

    public LastFmTag(Parcel source) {
        try {
            jObj = new JSONObject(source.readString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getName() {
        return jObj.optString("name");
    }

    public String getUrl() {
        return jObj.optString("url");
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(jObj.toString());
    }

    public static final Creator<LastFmTag> CREATOR = new Creator<LastFmTag>() {
        @Override
        public LastFmTag createFromParcel(Parcel source) {
            return new LastFmTag(source);
        }

        @Override
        public LastFmTag[] newArray(int size) {
            return new LastFmTag[size];
        }
    };

}
