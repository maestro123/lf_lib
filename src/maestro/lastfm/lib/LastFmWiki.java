package maestro.lastfm.lib;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Artyom on 27.08.2014.
 */
public class LastFmWiki implements Parcelable {

    private JSONObject jObj;

    public LastFmWiki(JSONObject _jObj) {
        jObj = _jObj;
    }

    public LastFmWiki(Parcel source) {
        try {
            jObj = new JSONObject(source.readString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getPublished() {
        return jObj.optString("published");
    }

    public String getSummary() {
        return jObj.optString("summary");
    }

    public String getContent() {
        return jObj.optString("content");
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(jObj.toString());
    }

    public static final Creator<LastFmWiki> CREATOR = new Creator<LastFmWiki>() {
        @Override
        public LastFmWiki createFromParcel(Parcel source) {
            return new LastFmWiki(source);
        }

        @Override
        public LastFmWiki[] newArray(int size) {
            return new LastFmWiki[size];
        }
    };

}
