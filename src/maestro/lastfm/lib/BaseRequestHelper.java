package maestro.lastfm.lib;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;

/**
 * Created by artyom on 8/27/14.
 */
public class BaseRequestHelper {

    public enum BASE_NETWORK_ERROR {
        UNKNOWN, CONNECTION, NO_RESUTL
    }

    public interface RequestListener {
        public void onResult(Object result);

        public boolean canDoRequest();
    }

    public static final Object doRequest(String urlPath) {
        Object result = null;
        try {
            URL url = new URL(urlPath);
            HttpURLConnection httpconn = (HttpURLConnection) url.openConnection();
            if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                StringBuilder builder = new StringBuilder();
                BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                String strLine = null;
                while ((strLine = input.readLine()) != null) {
                    builder.append(strLine);
                }
                input.close();
                result = builder.toString();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
            result = BASE_NETWORK_ERROR.CONNECTION;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result != null ? result : BASE_NETWORK_ERROR.UNKNOWN;
    }

}
