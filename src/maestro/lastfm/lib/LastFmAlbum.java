package maestro.lastfm.lib;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Artyom on 27.08.2014.
 */
public class LastFmAlbum implements Parcelable {

    private JSONObject jObj;

    private LastFmImage[] images;
    private LastFmTrack[] tracks;
    private LastFmTag[] tags;
    private LastFmWiki wiki;

    public LastFmAlbum(JSONObject _jObj) {
        jObj = _jObj.has("album") ? _jObj.optJSONObject("album") : _jObj;
        prepare();
    }

    public LastFmAlbum(Parcel source) {
        try {
            jObj = new JSONObject(source.readString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        prepare();
    }

    private final void prepare() {
        JSONArray array = jObj.optJSONArray("image");
        if (array != null && array.length() > 0) {
            final int size = array.length();
            images = new LastFmImage[size];
            for (int i = 0; i < size; i++) {
                images[i] = new LastFmImage(array.optJSONObject(i));
            }
        }
        JSONObject ljObject = jObj.optJSONObject("tracks");
        if (ljObject != null) {
            array = jObj.optJSONArray("track");
            if (array != null && array.length() > 0) {
                final int size = array.length();
                tracks = new LastFmTrack[size];
                for (int i = 0; i < size; i++) {
                    tracks[i] = new LastFmTrack(array.optJSONObject(i));
                }
            }
        }
        ljObject = jObj.optJSONObject("toptags");
        if (ljObject != null) {
            array = jObj.optJSONArray("tag");
            if (array != null && array.length() > 0) {
                final int size = array.length();
                tags = new LastFmTag[size];
                for (int i = 0; i < size; i++) {
                    tags[i] = new LastFmTag(array.optJSONObject(i));
                }
            }
        }
        if (jObj.has("wiki"))
            wiki = new LastFmWiki(jObj.optJSONObject("wiki"));
    }

    public LastFmWiki getWiki() {
        return wiki;
    }

    public LastFmImage[] getImages() {
        return images;
    }

    public LastFmImage getImage(String size) {
        if (images != null) {
            for (LastFmImage image : images) {
                if (image.getSize().equals(size))
                    return image;
            }
        }
        return null;
    }

    public LastFmImage getBiggestImage() {
        if (images != null && images.length > 0) {
            return images[images.length - 1];
        }
        return null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(jObj.toString());
    }

    public static final Creator<LastFmAlbum> CREATOR = new Creator<LastFmAlbum>() {
        @Override
        public LastFmAlbum createFromParcel(Parcel source) {
            return new LastFmAlbum(source);
        }

        @Override
        public LastFmAlbum[] newArray(int size) {
            return new LastFmAlbum[size];
        }
    };

}
