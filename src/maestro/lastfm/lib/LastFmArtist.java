package maestro.lastfm.lib;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Artyom on 27.08.2014.
 */
public class LastFmArtist implements Parcelable {

    private JSONObject jObj;
    private JSONObject jBio;

    private LastFmImage[] images;
    private LastFmArtist[] similarArtist;
    private LastFmTag[] tags;

    public LastFmArtist(JSONObject _jObj) {
        jObj = _jObj.has("artist") ? _jObj.optJSONObject("artist") : _jObj;
        prepare();
    }

    public LastFmArtist(Parcel source) {
        try {
            jObj = new JSONObject(source.readString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        prepare();
    }

    private final void prepare() {
        JSONArray array = jObj.optJSONArray("image");
        if (array != null && array.length() > 0) {
            final int size = array.length();
            images = new LastFmImage[size];
            for (int i = 0; i < size; i++) {
                images[i] = new LastFmImage(array.optJSONObject(i));
            }
        }
        JSONObject similar = jObj.optJSONObject("similar");
        if (similar != null) {
            array = similar.optJSONArray("artist");
            if (array != null && array.length() > 0) {
                final int size = array.length();
                similarArtist = new LastFmArtist[size];
                for (int i = 0; i < size; i++) {
                    similarArtist[i] = new LastFmArtist(array.optJSONObject(i));
                }
            }
        }
        JSONObject tag = jObj.optJSONObject("tags");
        if (tag != null) {
            array = tag.optJSONArray("tag");
            if (array != null && array.length() > 0) {
                final int size = array.length();
                tags = new LastFmTag[size];
                for (int i = 0; i < size; i++) {
                    tags[i] = new LastFmTag(array.optJSONObject(i));
                }
            }
        }
        jBio = jObj.optJSONObject("bio");
    }

    public LastFmImage[] getImages() {
        return images;
    }

    public LastFmImage getImage(String size) {
        if (images != null) {
            for (LastFmImage image : images) {
                if (image.getSize().equals(size))
                    return image;
            }
        }
        return null;
    }

    public String getName() {
        return jObj.optString("name");
    }

    public String getUrl() {
        return jObj.optString("url");
    }

    public String getPublished() {
        return jBio != null ? jBio.optString("published") : null;
    }

    public String getSummary() {
        return jBio != null ? jBio.optString("summary") : null;
    }

    public String getContent() {
        return jBio != null ? jBio.optString("content") : null;
    }

    public String getYearFormed() {
        return jBio != null ? jBio.optString("yearformed") : null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(jObj.toString());
    }

    public static final Creator<LastFmArtist> CREATOR = new Creator<LastFmArtist>() {
        @Override
        public LastFmArtist createFromParcel(Parcel source) {
            return new LastFmArtist(source);
        }

        @Override
        public LastFmArtist[] newArray(int size) {
            return new LastFmArtist[size];
        }
    };

}
