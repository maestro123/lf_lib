package maestro.lastfm.lib;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Artyom on 27.08.2014.
 */
public class LastFmImage implements Parcelable {

    private JSONObject jObj;

    public LastFmImage(JSONObject _jObj) {
        jObj = _jObj;
    }

    public LastFmImage(Parcel source) {
        try {
            jObj = new JSONObject(source.readString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getUrl() {
        return jObj.optString("#text");
    }

    public String getSize() {
        return jObj.optString("size");
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(jObj.toString());
    }

    public static final Creator<LastFmImage> CREATOR = new Creator<LastFmImage>() {
        @Override
        public LastFmImage createFromParcel(Parcel source) {
            return new LastFmImage(source);
        }

        @Override
        public LastFmImage[] newArray(int size) {
            return new LastFmImage[size];
        }
    };

}
