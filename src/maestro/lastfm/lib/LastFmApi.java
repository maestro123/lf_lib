package maestro.lastfm.lib;

import android.nfc.Tag;
import android.util.Log;
import maestro.lastfm.lib.data.LastFmDBHelper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.Locale;

/**
 * Created by Artyom on 27.08.2014.
 */
public class LastFmApi {

    public static final String TAG = LastFmApi.class.getSimpleName();

    private static final String API_KEY = "ac96873cb12d9b095abefd728145a0d9";

    public static final String BASE_URL = "http://ws.audioscrobbler.com/2.0/";

    public static final String PARAM_METHOD = "?method=";
    public static final String PARAM_API_KEY = "&api_key=";
    public static final String PARAM_ARTIST = "&artist=";
    public static final String PARAM_ALBUM = "&album=";
    public static final String PARAM_LANG = "&lang=";
    public static final String PARAM_TRACK = "&track=";
    public static final String PARAM_AUTOCOREECT = "&autocorrect=";

    public static final String METHOD_ARTIST_INFO = "artist.getInfo";
    public static final String METHOD_ALBUM_INFO = "album.getInfo";
    public static final String METHOD_TRACK_INFO = "track.getInfo";
    public static final String METHOD_ALBUM_SEARCH = "album.search";
    public static final String METHOD_TRACK_SEARCH = "track.search";

    public static final String FORMAT_JSON = "&format=json";

    public static final String IMAGE_SMALL = "small";
    public static final String IMAGE_MEDIUM = "medium";
    public static final String IMAGE_LARGE = "large";
    public static final String IMAGE_EXTRA_LARGE = "extralarge";
    public static final String IMAGE_MEGA = "mega";

    public static final String buildArtistInfoRequest(String artistName) {
        return getBaseBuilder(METHOD_ARTIST_INFO).append(PARAM_ARTIST)
                .append(URLEncoder.encode(artistName)).toString();
    }

    public static final String buildAlbumInfoRequest(String album, String artist) {
        return getBaseBuilder(METHOD_ALBUM_INFO).append(PARAM_ALBUM).append(URLEncoder.encode(album))
                .append(PARAM_ARTIST).append(URLEncoder.encode(artist)).toString();
    }

    public static final String buildTrackInfoRequest(String track, String artist) {
        return getBaseBuilder(METHOD_TRACK_INFO).append(PARAM_TRACK).append(URLEncoder.encode(track))
                .append(PARAM_ARTIST).append(URLEncoder.encode(artist)).toString();
    }

    public static final String buildAlbumSearchRequest(String album) {
        return getBaseBuilder(METHOD_ALBUM_SEARCH).append(PARAM_ALBUM).append(URLEncoder.encode(album)).toString();
    }

    public static final String buildTrackSearchRequest(String track) {
        return getBaseBuilder(METHOD_TRACK_SEARCH).append(PARAM_TRACK).append(URLEncoder.encode(track)).toString();
    }

    private static final StringBuilder getBaseBuilder(String method) {
        return new StringBuilder(BASE_URL).append(PARAM_METHOD).append(method)
                .append(PARAM_API_KEY).append(API_KEY).append(FORMAT_JSON)
                .append(PARAM_LANG).append(Locale.getDefault().getLanguage());
    }

    public static Object getAlbumInfo(final String url, final BaseRequestHelper.RequestListener listener, boolean async) {
        LastFmAlbum album = LastFmDBHelper.getInstance().findAlbumInfo(url);
        if (album == null) {
            MRequest request = new MRequest(url, listener, new GetAlbumInfoPostProcessor());
            if (async)
                request.async();
            else {
                request.block();
                return request.getResult();
            }
        } else {
            listener.onResult(album);
        }
        return null;
    }

    public static Object getArtistInfo(final String url, final BaseRequestHelper.RequestListener listener, boolean async) {
        LastFmArtist album = LastFmDBHelper.getInstance().findArtistInfo(url);
        if (album == null) {
            MRequest request = new MRequest(url, listener, new GetArtistInfoPostProcessor());
            if (async)
                request.async();
            else {
                request.block();
                return request.getResult();
            }
        } else {
            listener.onResult(album);
        }
        return null;
    }

    public static Object getTrackInfo(final String url, final BaseRequestHelper.RequestListener listener, boolean async) {
        LastFmArtist album = LastFmDBHelper.getInstance().findArtistInfo(url);
        if (album == null) {
            MRequest request = new MRequest(url, listener, new GetTrackInfoPostProcessor());
            if (async)
                request.async();
            else {
                request.block();
                return request.getResult();
            }
        } else {
            listener.onResult(album);
        }
        return null;
    }

    public static Object findAlbum(final String url, final BaseRequestHelper.RequestListener listener, boolean async) {
        MRequest localRequest = new MRequest(url, listener, new MRequest.PostProcessor() {
            @Override
            public Object postProcess(String url, String result) {
                try {
                    JSONObject rootObject = new JSONObject(result);
                    if (rootObject.has("results")) {
                        rootObject = rootObject.optJSONObject("results");
                        JSONArray results = rootObject.optJSONObject("albummatches").optJSONArray("album");
                        final int size = results.length();
                        if (size > 0) {
                            LastFmAlbum[] albums = new LastFmAlbum[size];
                            for (int i = 0; i < size; i++) {
                                albums[i] = new LastFmAlbum(results.optJSONObject(i));
                            }
                            return albums;
                        }
                    }
                    return BaseRequestHelper.BASE_NETWORK_ERROR.NO_RESUTL;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        });
        if (async) {
            localRequest.async();
        } else {
            localRequest.block();
            return localRequest.getResult();
        }
        return null;
    }

    public static Object findTrack(final String url, final BaseRequestHelper.RequestListener listener, boolean async) {
        MRequest localRequest = new MRequest(url, listener, new MRequest.PostProcessor() {
            @Override
            public Object postProcess(String url, String response) {
                try {
                    JSONObject rootObject = new JSONObject(response);
                    if (rootObject.has("results")) {
                        rootObject = rootObject.optJSONObject("results");
                        JSONArray results = rootObject.optJSONObject("trackmatches").optJSONArray("track");
                        final int size = results.length();
                        if (size > 0) {
                            LastFmTrack[] resultsArray = new LastFmTrack[size];
                            for (int i = 0; i < size; i++) {
                                resultsArray[i] = new LastFmTrack(results.getJSONObject(i));
                            }
                            return resultsArray;
                        }
                    }
                    return BaseRequestHelper.BASE_NETWORK_ERROR.NO_RESUTL;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        });
        if (async) {
            localRequest.async();
        } else {
            localRequest.block();
            return localRequest.getResult();
        }
        return null;
    }

    public static Object doRequest(String url) {
        return BaseRequestHelper.doRequest(url);
    }

    private static final class GetArtistInfoPostProcessor implements MRequest.PostProcessor {

        @Override
        public Object postProcess(String url, String result) {
            LastFmDBHelper.getInstance().addArtistInfo(url, result);
            try {
                return new LastFmArtist(new JSONObject(result));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private static final class GetAlbumInfoPostProcessor implements MRequest.PostProcessor {

        @Override
        public Object postProcess(String url, String result) {
            LastFmDBHelper.getInstance().addAlbumInfo(url, result);
            try {
                return new LastFmAlbum(new JSONObject(result));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

    }

    private static final class GetTrackInfoPostProcessor implements MRequest.PostProcessor {

        @Override
        public Object postProcess(String url, String result) {
            LastFmDBHelper.getInstance().addAlbumInfo(url, result);
            try {
                return new LastFmTrack(new JSONObject(result));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private static final class MRequest implements Runnable {

        private String url;
        private BaseRequestHelper.RequestListener listener;
        private PostProcessor postProcessor;
        private Object result;

        public interface PostProcessor {
            Object postProcess(String url, String object);
        }

        public MRequest(String url, BaseRequestHelper.RequestListener listener,
                        PostProcessor postProcessor) {
            this.url = url;
            this.listener = listener;
            this.postProcessor = postProcessor;
        }

        @Override
        public void run() {
            result = doRequest(url);
            if (result instanceof String && postProcessor != null) {
                result = postProcessor.postProcess(url, (String) result);
            }
            if (listener != null)
                listener.onResult(result != null
                        ? result : BaseRequestHelper.BASE_NETWORK_ERROR.UNKNOWN);
        }

        public Object getResult() {
            return result;
        }

        public void async() {
            new Thread(this).start();
        }

        public void block() {
            run();
        }

    }

}
