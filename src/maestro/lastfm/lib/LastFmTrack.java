package maestro.lastfm.lib;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Artyom on 27.08.2014.
 */
public class LastFmTrack implements Parcelable {

    private JSONObject jObj;
    private LastFmArtist artist;
    private LastFmAlbum album;
    private LastFmTag[] tags;
    private LastFmWiki wiki;

    public LastFmTrack(JSONObject _jObj) {
        jObj = _jObj.has("track") ? _jObj.optJSONObject("track") : _jObj;
        prepare();
    }

    public LastFmTrack(Parcel source) {
        try {
            jObj = new JSONObject(source.readString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        prepare();
    }

    private final void prepare() {
        JSONObject localJObject = jObj.optJSONObject("artist");
        if (localJObject != null) {
            artist = new LastFmArtist(localJObject);
        }
        localJObject = jObj.optJSONObject("album");
        if (localJObject != null) {
            album = new LastFmAlbum(localJObject);
        }
        localJObject = jObj.optJSONObject("toptags");
        if (localJObject != null) {
            JSONArray tagsArray = localJObject.optJSONArray("tag");
            final int size = tagsArray.length();
            tags = new LastFmTag[size];
            for (int i = 0; i < size; i++) {
                tags[i] = new LastFmTag(tagsArray.optJSONObject(i));
            }
        }
        localJObject = jObj.optJSONObject("wiki");
        if (localJObject != null)
            wiki = new LastFmWiki(localJObject);
    }

    public String getName() {
        return jObj.optString("name");
    }

    public String getDuration() {
        return jObj.optString("duration");
    }

    public String getMBID() {
        return jObj.optString("mbid");
    }

    public String getUrl() {
        return jObj.optString("url");
    }

    public LastFmArtist getArtist() {
        return artist;
    }

    public LastFmAlbum getAlbum() {
        return album;
    }

    public LastFmTag[] getTags() {
        return tags;
    }

    public LastFmWiki getWiki() {
        return wiki;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(jObj.toString());
    }

    public static final Creator<LastFmTrack> CREATOR = new Creator<LastFmTrack>() {
        @Override
        public LastFmTrack createFromParcel(Parcel source) {
            return new LastFmTrack(source);
        }

        @Override
        public LastFmTrack[] newArray(int size) {
            return new LastFmTrack[size];
        }
    };

}
