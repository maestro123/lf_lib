package maestro.lastfm.lib.data;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by artyom on 8/27/14.
 */
public class LastFmDatabase extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "lastfmData.db";

    private static final int DATABASE_VERSION = 1;

    public static final String TABLE_ALBUM_INFO = "tbl_album_info";
    public static final String TABLE_ARTIST_INFO = "tbl_artist_info";
    public static final String TABLE_TRACK_INFO = "tbl_track_info";
    public static final String TABLE_SEARCH_RESULTS = "tbl_search_results";

    public static final String KEY_ID = "_id";
    public static final String KEY_URL = "_url";
    public static final String KEY_CONTENT = "_content";
    public static final String KEY_DATE = "_date";

    private static final String CREATE_TABLE_ALBUM_INFO = "create table " + TABLE_ALBUM_INFO
            + "(" + KEY_ID + " INTEGER PRIMARY KEY, " + KEY_URL + " text, " + KEY_CONTENT + " text," + KEY_DATE + " text);";

    private static final String CREATE_TABLE_ARTIST_INFO = "create table " + TABLE_ARTIST_INFO
            + "(" + KEY_ID + " INTEGER PRIMARY KEY, " + KEY_URL + " text, " + KEY_CONTENT + " text," + KEY_DATE + " text);";

    private static final String CREATE_TABLE_TRACK_INFO = "create table " + TABLE_TRACK_INFO
            + "(" + KEY_ID + " INTEGER PRIMARY KEY, " + KEY_URL + " text, " + KEY_CONTENT + " text," + KEY_DATE + " text);";

    private static final String CREATE_TABLE_SEARCH_RESULTS = "create table " + TABLE_SEARCH_RESULTS
            + "(" + KEY_ID + " INTEGER PRIMARY KEY, " + KEY_URL + " text, " + KEY_CONTENT + " text," + KEY_DATE + " text);";

    public LastFmDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_ALBUM_INFO);
        sqLiteDatabase.execSQL(CREATE_TABLE_ARTIST_INFO);
        sqLiteDatabase.execSQL(CREATE_TABLE_TRACK_INFO);
        sqLiteDatabase.execSQL(CREATE_TABLE_SEARCH_RESULTS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_ALBUM_INFO);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_ARTIST_INFO);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_TRACK_INFO);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_SEARCH_RESULTS);
        onCreate(sqLiteDatabase);
    }
}
