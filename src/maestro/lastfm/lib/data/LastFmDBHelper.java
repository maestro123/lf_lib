package maestro.lastfm.lib.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import maestro.lastfm.lib.LastFmAlbum;
import maestro.lastfm.lib.LastFmArtist;
import maestro.lastfm.lib.LastFmTrack;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

/**
 * Created by artyom on 8/27/14.
 */
public class LastFmDBHelper {

    public static final String TAG = LastFmDatabase.class.getSimpleName();

    private static volatile LastFmDBHelper instance;

    public static final synchronized LastFmDBHelper getInstance() {
        LastFmDBHelper localInstance = instance;
        if (localInstance == null) {
            synchronized (LastFmDBHelper.class) {
                localInstance = instance;
                if (localInstance == null) {
                    localInstance = instance = new LastFmDBHelper();
                }
            }
        }
        return localInstance;
    }

    LastFmDBHelper() {
    }

    private Context mContext;
    private LastFmDatabase mDatabaseHelper;
    private SQLiteDatabase mDatabase;

    public void init(Context context) {
        mContext = context;
        mDatabaseHelper = new LastFmDatabase(context);
        open();
    }

    public boolean isInitialized() {
        return mContext != null;
    }

    private void open() {
        mDatabase = mDatabaseHelper.getWritableDatabase();
    }

    private void close() {
        mDatabase.close();
    }

    public void addAlbumInfo(String url, String content) {
        addRow(LastFmDatabase.TABLE_ALBUM_INFO, url, content);
    }

    public LastFmAlbum findAlbumInfo(String url) {
        JSONObject cached = findRow(LastFmDatabase.TABLE_ALBUM_INFO, url);
        return cached != null ? new LastFmAlbum(cached) : null;
    }

    public void addArtistInfo(String url, String content) {
        addRow(LastFmDatabase.TABLE_ARTIST_INFO, url, content);
    }

    public LastFmArtist findArtistInfo(String url) {
        JSONObject cached = findRow(LastFmDatabase.TABLE_ARTIST_INFO, url);
        return cached != null ? new LastFmArtist(cached) : null;
    }

    public void addTrackInfo(String url, String content) {
        addRow(LastFmDatabase.TABLE_TRACK_INFO, url, content);
    }

    public LastFmTrack findTrackInfo(String url) {
        JSONObject cached = findRow(LastFmDatabase.TABLE_TRACK_INFO, url);
        return cached != null ? new LastFmTrack(cached) : null;
    }

    public void addSearchResult(String url, String content) {
        addRow(LastFmDatabase.TABLE_SEARCH_RESULTS, url, content);
    }

    public void findSearchResult(String url) {
        //TODO: implement
    }

    private final void addRow(String table, String url, String content) {
        ContentValues values = new ContentValues();
        values.put(LastFmDatabase.KEY_URL, prepareString(url));
        values.put(LastFmDatabase.KEY_CONTENT, prepareString(content));
        values.put(LastFmDatabase.KEY_DATE, Calendar.getInstance().getTime().toString());
//        open();
        mDatabase.insert(table, null, values);
//        close();
    }

    private final JSONObject findRow(String table, String url) {
//        open();
        try {
            Cursor cursor = mDatabase.query(table, null, LastFmDatabase.KEY_URL + " = " + prepareString(url),
                    null, null, null, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                try {
                    return new JSONObject(rePrepareString(cursor.getString(cursor.getColumnIndexOrThrow(LastFmDatabase.KEY_CONTENT))));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            cursor.close();
            return null;
        } finally {
//            close();
        }
    }

    private String prepareString(String value) {
        return new StringBuilder().append("'").append(value).append("'").toString();
    }

    private String rePrepareString(String value) {
        if (value.startsWith("'"))
            return value.substring(1, value.length() - 1);
        return value;
    }

}
